# btlabz-docs-umami-monitoring

## Overview

This documents describes a possible approach to umami monitoring.

Notice: there is a hard requirements to have 3x nodes. I assume those nodes are separate servers.

Requirements:

- monitor resources of VM
- monitor API
- monitor SSL certificates
- monitor DB
- monitor reachability

## Solution

Prometheus + Grafana + Alert Manager for custom monitoring.

AWS CloudWatch Metrics + CloudWatch Alerts + CloudWatch Logs for cloud native metrics.

Let's fulfil requirements as follows:

- VM Resources: prometheus node-exporter, AWS CW EC2 Compute, Storage and Netwrok metrics.
- API: prometheus blackbox-exporter, AWS CW ALB Metrics.
- SSL Certificates: prometheus cert-exporter
- DB: prometheus postgres-exporter, AWS CW RDS Metrics
- Reachability: https://www.site24x7.com/ high level monitoring, location reachability

Generally, we need to measure 4 golden signals:

- Latency: AWS CLoudFront Response Times, AWS ALB Target and Client response times, Local reverse proxy latency on the each node.
- Traffic: Number of request per ALB, per Target, number of requests on CDN
- Errors: CDN client & target error rates, AWS ALB client and target error rates, per node error rates.
- Saturation: CPU load, Mem usage, Storage usage, Network bandwidth usage, RDS Connections usage etc

## Deployment

Let's use Managed Prometheus Storage and Grafana on AWS.

See: [Amazon Managed Prometheus](https://aws.amazon.com/prometheus/)

See: [Amazon Managed Grafana](https://aws.amazon.com/grafana/)

Let' use combination of terraform, packer, ansible to deploy and manage solution.

- Umami node: Packer + Ansible to bake AMI. Ansible playbook to change monitoring configuration ad-hoc.
- Prometheus node (blackbox-exporter, cert-exporter, postgres-exporter etc, prometheus scraper): Packer + Ansible to bake AMI, Ansible playbook to change monitoring configuration ad-hoc.
- Prometheus backend, Grafana: terraform+cloudformation

Slack alerting: [AM2Slack](https://aws.amazon.com/blogs/mt/how-to-integrate-amazon-managed-service-for-prometheus-with-slack/)

## Diagrams

**Image: umami monitoring**

![umami-mon](umami-mon.png)

## TODOs

- Detailed metrics and SLI definitions (4x golden signals)
- AWS Prometheus + Grafana terraform/cloudformation
- Slack notificatioins lambda + terraform
- Packer + Ansible for umami node
- Packer + Ansible for prometheus scraper node
- Grafana Dashboards

## References

- [T1: apache log analisys with python, bash, sqlite and docker](https://gitlab.com/btlabz-other/btlabz-docker-httpd2sqlite-poc)
- [T2: terraform node](https://gitlab.com/btlabz-umami/terraform-aws-btlabz-umami-dev-layer) and [ansible playbook](https://gitlab.com/btlabz-umami/btlabz-ansible-umami-playbook)
- [T3: Umami HA and resilience considerations](https://gitlab.com/btlabz-umami/btlabz-docs-umami-ha)
- [T4: Umami monitoring considerations](https://gitlab.com/btlabz-umami/btlabz-docs-umami-monitoring)
